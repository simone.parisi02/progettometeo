/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainPackage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author arturo.galvagni
 */
public class JaxbUnmarshal {

    String filePath;
    String context;
    JAXBContext jaxbContext;
    InputStream InpStr;

    public JaxbUnmarshal(String filePath, String context) {
        this.filePath = filePath;
        this.context = context;
        try {
            jaxbContext = JAXBContext.newInstance(context);
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public Object getUnmarshalledObject() throws FileNotFoundException {
        Unmarshaller unmarshaller = null;
        Object objectJAXB = null;
        try {
            unmarshaller = jaxbContext.createUnmarshaller();
            objectJAXB = unmarshaller.unmarshal(new FileInputStream(filePath));
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return objectJAXB;
    }
}
