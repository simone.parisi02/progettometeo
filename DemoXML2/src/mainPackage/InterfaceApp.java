package mainPackage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import mainPackage.JaxbUnmarshal;
import it.meteotrentino.ArrayOfAnagrafica;
import it.meteotrentino.ArrayOfAnagrafica.Anagrafica;
import java.awt.BorderLayout;
import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.*;
import javax.xml.bind.*;
import javax.xml.xpath.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Element;

/**
 *
 * @author arturo.galvagni
 */
public class InterfaceApp extends javax.swing.JFrame {
    
    DocumentBuilder documentBuilder;        
    ArrayOfAnagrafica v;
    //Costruttore interfaccia
    public InterfaceApp() throws ParserConfigurationException{
        super("Progetto Meteo Simone Parisi, Manuel Simoncelli");
        initComponents();
        setVisible(true);
        setResizable(false);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        documentBuilder = factory.newDocumentBuilder();
        System.out.println("Interfaccia avviata");
    }
    
    //Carica dati stazioni
    public void caricaDati(ArrayOfAnagrafica v) { 
        this.v = v;
        for (Anagrafica a : v.getAnagrafica()){
            if(a.getFine() == null || a.getFine().equals("")){
                jComboBox1.addItem(a.getNome());
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();
        chartTemperature = new javax.swing.JPanel();
        chartVento = new javax.swing.JPanel();
        chartPrecipitazioni = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Load");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        chartTemperature.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout chartTemperatureLayout = new javax.swing.GroupLayout(chartTemperature);
        chartTemperature.setLayout(chartTemperatureLayout);
        chartTemperatureLayout.setHorizontalGroup(
            chartTemperatureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        chartTemperatureLayout.setVerticalGroup(
            chartTemperatureLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 183, Short.MAX_VALUE)
        );

        chartVento.setBackground(new java.awt.Color(255, 255, 255));
        chartVento.setPreferredSize(new java.awt.Dimension(978, 183));

        javax.swing.GroupLayout chartVentoLayout = new javax.swing.GroupLayout(chartVento);
        chartVento.setLayout(chartVentoLayout);
        chartVentoLayout.setHorizontalGroup(
            chartVentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 978, Short.MAX_VALUE)
        );
        chartVentoLayout.setVerticalGroup(
            chartVentoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 183, Short.MAX_VALUE)
        );

        chartPrecipitazioni.setBackground(new java.awt.Color(255, 255, 255));
        chartPrecipitazioni.setPreferredSize(new java.awt.Dimension(0, 183));

        javax.swing.GroupLayout chartPrecipitazioniLayout = new javax.swing.GroupLayout(chartPrecipitazioni);
        chartPrecipitazioni.setLayout(chartPrecipitazioniLayout);
        chartPrecipitazioniLayout.setHorizontalGroup(
            chartPrecipitazioniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        chartPrecipitazioniLayout.setVerticalGroup(
            chartPrecipitazioniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 183, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox1, 0, 899, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(chartTemperature, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chartVento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chartPrecipitazioni, javax.swing.GroupLayout.DEFAULT_SIZE, 978, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chartTemperature, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chartVento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chartPrecipitazioni, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        for(Anagrafica vTmp : v.getAnagrafica()){
            String tmp = jComboBox1.getSelectedItem().toString();
            if(tmp.equals(vTmp.getNome())){
                try {
                    caricaDatiStazione(vTmp.getCodice());
                } catch (IOException ex) {
                    Logger.getLogger(InterfaceApp.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SAXException ex) {
                    Logger.getLogger(InterfaceApp.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    //Carica dati singola stazione
    public void caricaDatiStazione(String codice) throws MalformedURLException, IOException, SAXException {       
               
        DefaultCategoryDataset datasetTemperature = new DefaultCategoryDataset();    
        DefaultCategoryDataset datasetPrecipitazioni = new DefaultCategoryDataset();  
        DefaultCategoryDataset datasetVento = new DefaultCategoryDataset();  
       
        Document tmpDocument = documentBuilder.parse(new URL("http://dati.meteotrentino.it/service.asmx/ultimiDatiStazione?codice=" + codice).openStream());
        
        //Ottenimento dati temperature
        NodeList listTemperature = tmpDocument.getElementsByTagName("temperatura_aria");                           
        for(int i = 0; i < listTemperature.getLength(); i++){
            NodeList tmpElement = ((Element)listTemperature.item(i)).getElementsByTagName("temperatura");
            // write(tmpElement.item(0).getTextContent());
            datasetTemperature.setValue(Double.valueOf(tmpElement.item(0).getTextContent()), "Temperatura", "Value: " + i);         
        }
        
        //Ottenimento dati precipitazioni
        NodeList listPrecipitazioni = tmpDocument.getElementsByTagName("precipitazione");              
        for(int i = 0; i < listPrecipitazioni.getLength(); i++){
            NodeList tmpElement = ((Element)listPrecipitazioni.item(i)).getElementsByTagName("pioggia");
            // write(tmpElement.item(0).getTextContent());
            datasetPrecipitazioni.setValue(Double.valueOf(tmpElement.item(0).getTextContent()), "Precipitazioni", "Value: " + i);
        }
        
        //Ottenimento dati vento
        NodeList listVento = tmpDocument.getElementsByTagName("vento_al_suolo");              
        for(int i = 0; i < listVento.getLength(); i++){
            NodeList tmpElement = ((Element)listVento.item(i)).getElementsByTagName("v");
            // write(tmpElement.item(0).getTextContent());
            datasetVento.setValue(Double.valueOf(tmpElement.item(0).getTextContent()), "Vento", "Value: " + i);
        }
        
        //Creazione grafico temperature
        JFreeChart oChartTemperature = ChartFactory.createLineChart("Temperatura", "Orario", "Gradi", datasetTemperature,PlotOrientation.VERTICAL, false,false,false);
        ChartPanel oPanelTemperature = new ChartPanel(oChartTemperature);
        chartTemperature.removeAll();
        chartTemperature.setLayout(new java.awt.BorderLayout());
        chartTemperature.add(oPanelTemperature);
        chartTemperature.validate();
        
        //Creazione grafico precipitazioni
        JFreeChart oChartPrecipitazioni = ChartFactory.createLineChart("Precipitazioni", "Orario", "mm", datasetPrecipitazioni, PlotOrientation.VERTICAL,false,false,false);        
        ChartPanel oPanelPrecipitazioni = new ChartPanel(oChartPrecipitazioni);       
        chartVento.removeAll();
        chartVento.setLayout(new java.awt.BorderLayout());
        chartVento.add(oPanelPrecipitazioni);
        chartVento.validate();  
        
        //Creazione grafico Vento
        JFreeChart oChartVento = ChartFactory.createLineChart("Vento", "Orario", "Km/h", datasetVento, PlotOrientation.VERTICAL,false,false,false);
        ChartPanel oPanelVento = new ChartPanel(oChartVento);      
        chartPrecipitazioni.removeAll();
        chartPrecipitazioni.setLayout(new java.awt.BorderLayout());
        chartPrecipitazioni.add(oPanelVento);
        chartPrecipitazioni.validate(); 
    }
       
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfaceApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfaceApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfaceApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfaceApp.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new InterfaceApp().setVisible(true);
                } catch (ParserConfigurationException ex) {
                    Logger.getLogger(InterfaceApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel chartPrecipitazioni;
    private javax.swing.JPanel chartTemperature;
    private javax.swing.JPanel chartVento;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox1;
    // End of variables declaration//GEN-END:variables
}
