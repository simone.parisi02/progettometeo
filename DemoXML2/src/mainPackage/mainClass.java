package mainPackage;

import it.meteotrentino.ArrayOfAnagrafica;
import it.meteotrentino.ArrayOfAnagrafica.Anagrafica;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import javax.xml.parsers.ParserConfigurationException;

public class mainClass {
    public static void main(String[] args) throws FileNotFoundException, ParserConfigurationException, MalformedURLException, IOException {
        
        // Download ultima versione file stazioni
        URL website = new URL("http://dati.meteotrentino.it/service.asmx/listaStazioni");
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        FileOutputStream fos = new FileOutputStream("src\\mainPackage\\listaStazioni.xml");
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        System.out.println("File stazioni scaricato");
        
        // TODO code application logic here
        String filePath = "src\\mainPackage\\listaStazioni.xml";
        String context = "it.meteotrentino";
        JaxbUnmarshal unmarshall = new JaxbUnmarshal(filePath, context);
        ArrayOfAnagrafica v = (ArrayOfAnagrafica) unmarshall.getUnmarshalledObject();
        System.out.println("File stazioni letto");
        
        /* for (int i = 0; i < v.getAnagrafica().size(); i++)
        {
            System.out.print(v.getAnagrafica().get(i).getNome()+"-->");
            System.out.println(v.getAnagrafica().get(i).getCodice());
        } 
        */
        
        InterfaceApp interfaceApp = new InterfaceApp();
        interfaceApp.caricaDati(v);

    } 
}
