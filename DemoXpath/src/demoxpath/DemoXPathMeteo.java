/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoxpath;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
/**
 *
 * @author Arturo
 */
public class DemoXPathMeteo {

    /**
     * @param args the command line arguments
     */
    
     public static void main(String[] args) throws Exception {
     // TODO code application logic here
        
     //Build DOM
 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true); // never forget this!
        DocumentBuilder builder = factory.newDocumentBuilder();
        // file senza header namespace 
       // Document doc = builder.parse("stazioneT0154-2.xml");
       
        // file con header namespace
        Document doc = builder.parse("stazioneT0154.xml");
        
        //Create XPath
        XPathFactory xpathfactory = XPathFactory.newInstance();
        XPath xpath = xpathfactory.newXPath();
               
        System.out.println("n//1 valori di temperatura");  
        // 1) temperature
        //XPathExpression expr = xpath.compile("//temperature/temperatura_aria/temperatura/text()");
        XPathExpression expr = xpath.compile("//*[local-name()='temperatura']/text()");
       
        Object result = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
           System.out.println(nodes.item(i).getNodeValue());
        }
        
        System.out.println("n//2 valori di precipitazione");
        // 2) precipitazioni
        //expr = xpath.compile("//precipitazioni/precipitazione/pioggia/text()");
        expr = xpath.compile("//*[local-name()='pioggia']/text()");
       
        result = expr.evaluate(doc, XPathConstants.NODESET);
        nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
            System.out.println(nodes.item(i).getNodeValue());
        }
 
        System.out.println("n//3) Vento al suolo");
 
        // 3) Vento al suolo
       // expr = xpath.compile("//venti/vento_al_suolo/v/text()");
        expr = xpath.compile("//*[local-name()='v']/text()");
 
        result = expr.evaluate(doc, XPathConstants.NODESET);
        nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
            System.out.println(nodes.item(i).getNodeValue());
        }
 
        System.out.println("n//4) radiazione globale");
 
        // 4) radiazione globale
        //expr = xpath.compile("//radiazione/radiazioneglobale/rsg/text()");
        expr = xpath.compile("//*[local-name()='rsg']/text()");
 
        result = expr.evaluate(doc, XPathConstants.NODESET);
        nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
            System.out.println(nodes.item(i).getNodeValue());
        }
 
        System.out.println("n//5) umidità relativa");
 
        // 5) umidità relativa
  //      expr = xpath.compile("//umidita_relativa/umidita_relativa/rh/text()");
        expr = xpath.compile("//*[local-name()='rh']/text()");
 
        result = expr.evaluate(doc, XPathConstants.NODESET);
        nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
            System.out.println(nodes.item(i).getNodeValue());
        }
 
      
       //0) temperature
       expr = xpath.compile("//*[local-name()='temperatura_aria']");
              
       result = expr.evaluate(doc, XPathConstants.NODESET);
       nodes = (NodeList) result;
        for (int i = 0; i < nodes.getLength(); i++) {
           Element eElement = (Element) nodes.item(i);
           System.out.print(eElement.getElementsByTagName("data").item(0).getTextContent()+" ->");                  
           System.out.println(eElement.getElementsByTagName("temperatura").item(0).getTextContent()); 
       
    }
      
    }
}
    

